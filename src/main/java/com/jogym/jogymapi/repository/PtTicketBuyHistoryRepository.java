package com.jogym.jogymapi.repository;

import com.jogym.jogymapi.entity.Member;
import com.jogym.jogymapi.entity.PtTicketBuyHistory;
import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.enums.BuyStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PtTicketBuyHistoryRepository extends JpaRepository<PtTicketBuyHistory, Long> {
    long countByMemberAndBuyStatus(Member member, BuyStatus buyStatus);

    Optional<PtTicketBuyHistory> findByMember_StoreMemberAndId(StoreMember storeMember, Long id);

    Page<PtTicketBuyHistory> findAllByMember_IdAndMember_StoreMemberOrderByIdDesc(Long memberId, StoreMember storeMember, Pageable pageable);

}
