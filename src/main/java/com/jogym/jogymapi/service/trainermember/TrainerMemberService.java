package com.jogym.jogymapi.service.trainermember;

import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.entity.TrainerMember;
import com.jogym.jogymapi.exception.CMissingDataException;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.trainermember.TrainerMemberItem;
import com.jogym.jogymapi.model.trainermember.TrainerMemberRequest;
import com.jogym.jogymapi.model.trainermember.TrainerMemberResponse;
import com.jogym.jogymapi.model.trainermember.TrainerMemberUpdateRequest;
import com.jogym.jogymapi.repository.TrainerMemberRepository;
import com.jogym.jogymapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.aspectj.weaver.NewConstructorTypeMunger;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TrainerMemberService {
    private final TrainerMemberRepository trainerMemberRepository;

    public TrainerMember getOriginTrainer(long trainerId) {
        return trainerMemberRepository.findById(trainerId).orElseThrow(CMissingDataException::new);
    }

    // 직원 등록
    public void setTrainer(StoreMember storeMember, TrainerMemberRequest request) {
        TrainerMember trainerMember = new TrainerMember.TrainerMemberBuilder(storeMember, request).build();
        trainerMemberRepository.save(trainerMember);
    }

    // 직원 리스트 ( 페이징 )
    public ListResult<TrainerMemberItem> getTrainerList(StoreMember storeMember, int page) {
        Page<TrainerMember> originList = trainerMemberRepository.findAllByStoreMemberAndIsEnabledOrderByDateCreateDesc(storeMember,true, ListConvertService.getPageable(page));

        List<TrainerMemberItem> result = new LinkedList<>();

        for (TrainerMember trainerMember : originList.getContent()) {
            result.add(new TrainerMemberItem.Builder(trainerMember).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    // 직원 상세 정보 리스트
    public TrainerMemberResponse getTrainer(long trainerId, StoreMember storeMember){
        TrainerMember trainerMember = trainerMemberRepository.findByIdAndStoreMember(trainerId,storeMember).orElseThrow(CMissingDataException::new);  // 데이터가 없습니다. 데이터가 없으면 던지기.
        if(!trainerMember.getIsEnabled()) throw new CMissingDataException(); // 이미 탈퇴한 직원. 탈퇴한
        return new TrainerMemberResponse.Builder(trainerMember).build();
    }

    // 직원 수정
    public void putTrainer(long trainerId, StoreMember storeMember, TrainerMemberUpdateRequest request) {
        TrainerMember originData = trainerMemberRepository.findById(trainerId).orElseThrow(CMissingDataException::new); // 트레이너 정보가 다르면 던지기.
        if(!originData.getIsEnabled()) throw new CMissingDataException(); // 이미 탈퇴한 직원
        if(storeMember.getId() != originData.getStoreMember().getId()) throw new CMissingDataException(); // 가맹점이 다릅니다. 가맹점 아이디가 다르면 던지기.
                            // != 는 연산자임.
                            // 두 피 연산자의 값이 다른지를 검사하는 용도.
        originData.putTrainerMember(request);
        trainerMemberRepository.save(originData);
    }

    // 직원 삭제
    public void putTrainerDelete(long trainerId, StoreMember storeMember) {
        TrainerMember originData = trainerMemberRepository.findById(trainerId).orElseThrow(CMissingDataException::new); // 트레이너 정보가 다르면 던지기.
        if(!originData.getIsEnabled()) throw new CMissingDataException(); // 이미 탈퇴한 직원
        if(storeMember.getId() != originData.getStoreMember().getId()) throw new CMissingDataException(); // 가맹점이 다릅니다. 가맹점 아이디가 다르면 던지기.
                            // != 는 연산자임.
                            // 두 피 연산자의 값이 다른지를 검사하는 용도.
        originData.putTrainerMemberDelete();
        trainerMemberRepository.save(originData);
    }
}
