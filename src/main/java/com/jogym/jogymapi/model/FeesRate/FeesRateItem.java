package com.jogym.jogymapi.model.FeesRate;

import com.jogym.jogymapi.entity.FeesRate;
import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import com.jogym.jogymapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FeesRateItem {
    @ApiModelProperty(notes = "시퀀스", required = true)
    @NotNull
    private Long id;

    @ApiModelProperty(notes = "수정일시", required = true)
    @NotNull
    private String dateChange;

    @ApiModelProperty(notes = "수수료율", required = true)
    @NotNull
    private Double feesRate;

    private FeesRateItem(FeesRateBuilder builder) {
        this.id = builder.id;
        this.dateChange = builder.dateChange;
        this.feesRate = builder.feesRate;
    }

    public static class FeesRateBuilder implements CommonModelBuilder<FeesRateItem>  {
        private final Long id;
        private final String dateChange;
        private final Double feesRate;

        public FeesRateBuilder(FeesRate feesRate) {
            this.id = feesRate.getId();
            this.dateChange = CommonFormat.convertLocalDateTimeToString(feesRate.getDateChange());
            this.feesRate = feesRate.getFeesRate();
        }

        @Override
        public FeesRateItem build() {
            return new FeesRateItem(this);
        }
    }
}
