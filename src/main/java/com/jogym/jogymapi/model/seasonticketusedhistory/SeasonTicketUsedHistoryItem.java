package com.jogym.jogymapi.model.seasonticketusedhistory;

import com.jogym.jogymapi.entity.Member;
import com.jogym.jogymapi.entity.SeasonTicket;
import com.jogym.jogymapi.entity.SeasonTicketUsedHistory;
import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import com.jogym.jogymapi.lib.CommonFormat;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SeasonTicketUsedHistoryItem {
    private Long id;

    private Long member;

    private String dateCreate;

    private String ticketName;

    private SeasonTicketUsedHistoryItem(Builder builder) {
        this.id = builder.id;
        this.member = builder.member;
        this.dateCreate = builder.dateCreate;
        this.ticketName = builder.ticketName;
    }

    public static class Builder implements CommonModelBuilder<SeasonTicketUsedHistoryItem> {

        private final Long id;

        private final Long member;

        private final String dateCreate;

        private final String ticketName;

        public Builder(Member member, SeasonTicketUsedHistory seasonTicketUsedHistory) {
            this.id = seasonTicketUsedHistory.getId();
            this.member = member.getId();
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(seasonTicketUsedHistory.getDateCreate());
            this.ticketName = seasonTicketUsedHistory.getSeasonTicketBuyHistory().getSeasonTicket().getTicketName();
        }
        @Override
        public SeasonTicketUsedHistoryItem build() {
            return new SeasonTicketUsedHistoryItem(this);
        }
    }
}
