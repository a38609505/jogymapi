package com.jogym.jogymapi.repository;

import com.jogym.jogymapi.entity.Member;
import com.jogym.jogymapi.entity.SeasonTicketUsedHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Optional;

public interface SeasonTicketUsedHistoryRepository extends JpaRepository<SeasonTicketUsedHistory, Long> {
    Page<SeasonTicketUsedHistory> findAllByIdGreaterThanEqualOrderByIdDesc (long id, Pageable pageable);

    Optional<SeasonTicketUsedHistory> findBySeasonTicketBuyHistory_MemberAndDateLast (Member member, LocalDate localDate);
}
