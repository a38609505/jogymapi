package com.jogym.jogymapi.service.seasonticketusedhistory;

import com.jogym.jogymapi.entity.Member;
import com.jogym.jogymapi.entity.SeasonTicketBuyHistory;
import com.jogym.jogymapi.entity.SeasonTicketUsedHistory;
import com.jogym.jogymapi.exception.CAlreadyDateCheckTodayException;
import com.jogym.jogymapi.exception.CAlreadyUsingSeasonTicketException;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.seasonticketusedhistory.SeasonTicketUsedHistoryItem;
import com.jogym.jogymapi.repository.SeasonTicketUsedHistoryRepository;
import com.jogym.jogymapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SeasonTicketUsedHistoryService {
    private final SeasonTicketUsedHistoryRepository seasonTicketUsedHistoryRepository;

    // 정기권 사용 내역 등록
    public void setSeasonTicketUsedHistory(SeasonTicketBuyHistory seasonTicketBuyHistory) {
        if (!checkToday(seasonTicketBuyHistory.getMember())) throw new CAlreadyDateCheckTodayException(); // 만약

        SeasonTicketUsedHistory seasonTicketUsedHistory = new SeasonTicketUsedHistory.SeasonTicketUsedHistoryBuilder(seasonTicketBuyHistory).build();

        seasonTicketUsedHistoryRepository.save(seasonTicketUsedHistory);
    }

    // 정기권 사용 내역 리스트 ( 페이징 )
    public ListResult<SeasonTicketUsedHistoryItem> getData(int page, Member member) {
        Page<SeasonTicketUsedHistory> originList = seasonTicketUsedHistoryRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(page));

        List<SeasonTicketUsedHistoryItem> result = new LinkedList<>();

        for (SeasonTicketUsedHistory seasonTicketUsedHistory : originList) {
            result.add(new SeasonTicketUsedHistoryItem.Builder(member, seasonTicketUsedHistory).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    // 방문 내역 설정. ( flase에서 ture로 )
    private boolean checkToday(Member member){
        Optional<SeasonTicketUsedHistory> check = seasonTicketUsedHistoryRepository.findBySeasonTicketBuyHistory_MemberAndDateLast(member, LocalDate.now());

        if (check.isPresent()) return false; // 만약에 check 안에 있는 자료의 정보와 repository 한테 가져 오라고 요청한 자료의 정보가 일치할 경우 던짐

        return true;
    }
}
