package com.jogym.jogymapi.service.seasonticket;

import com.jogym.jogymapi.entity.SeasonTicket;
import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.exception.CMissingDataException;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.seasonticket.SeasonTicketItem;
import com.jogym.jogymapi.model.seasonticket.SeasonTicketRequest;
import com.jogym.jogymapi.model.seasonticket.SeasonTicketUpdateRequest;
import com.jogym.jogymapi.repository.SeasonTicketRepository;
import com.jogym.jogymapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SeasonTicketService {
    private final SeasonTicketRepository seasonTicketRepository;

    public SeasonTicket getOriginSeasonTicket(long id) {
        return seasonTicketRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    // 정기권 티켓 등록
    public void setSeasonTicket(StoreMember storeMember, SeasonTicketRequest request) {
        SeasonTicket seasonTicket = new SeasonTicket.SeasonTicketBuilder(storeMember, request).build();

        seasonTicketRepository.save(seasonTicket);
    }

    // 정기권 티켓 리스트
    public ListResult<SeasonTicketItem> getSeasonTicket(StoreMember storeMember, int page) {
        Page<SeasonTicket> originList = seasonTicketRepository.findAllByStoreMemberAndIsEnabledOrderByDateCreateDesc(storeMember, true, ListConvertService.getPageable(page));

        List<SeasonTicketItem> result = new LinkedList<>();

        for (SeasonTicket seasonTicket : originList.getContent()) {
            result.add(new SeasonTicketItem.Builder(seasonTicket).build());
        }

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    // 정기권 티켓 수정
    public void putSeasonTicket(long seasonTicketId, StoreMember storeMember, SeasonTicketUpdateRequest request) {
        SeasonTicket seasonTicket = seasonTicketRepository.findById(seasonTicketId).orElseThrow(CMissingDataException::new); // 정기권 티켓 정보가 다르면 던지기.

        if(seasonTicket.getStoreMember().getId() != storeMember.getId()) throw new CMissingDataException(); // 가맹점 정보가 다릅니다.
                                                // != 는 연산자임.
                                                // 두 피 연산자의 값이 다른지를 검사하는 용도.
        if(!seasonTicket.getIsEnabled()) throw new CMissingDataException(); // 삭제한 정기권 입니다.

        seasonTicket.putSeasonTicket(request);

        seasonTicketRepository.save(seasonTicket);
    }


    // 정기권 티켓 삭제
    public void putSeasonTicketDelete(long seasonTicketId, StoreMember storeMember) {
        SeasonTicket seasonTicket = seasonTicketRepository.findById(seasonTicketId).orElseThrow(CMissingDataException::new); // 정기권 티켓 정보가 다르면 다르기.
        if(seasonTicket.getStoreMember().getId() != storeMember.getId()) throw new CMissingDataException(); // 가맹점 정보가 다릅니다.
        if(!seasonTicket.getIsEnabled()) throw new CMissingDataException(); // 삭제한 정기권 입니다.
        seasonTicket.putSeasonTicketDelete();
        seasonTicketRepository.save(seasonTicket);
    }
// 일일권 유무 검사
//    public boolean getIsExistsSeasonTicketOfDay(StoreMember storeMember){
//
//    }

}
