package com.jogym.jogymapi.service.storemember;

import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.exception.CMissingDataException;
import com.jogym.jogymapi.repository.StoreMemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StoreMemberService {
    private final StoreMemberRepository storeMemberRepository;

    public StoreMember setStore(long id) {
        return storeMemberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }
}
