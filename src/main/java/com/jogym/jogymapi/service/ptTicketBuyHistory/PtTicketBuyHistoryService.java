package com.jogym.jogymapi.service.ptTicketBuyHistory;

import com.jogym.jogymapi.entity.Member;
import com.jogym.jogymapi.entity.PtTicket;
import com.jogym.jogymapi.entity.PtTicketBuyHistory;
import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.enums.BuyStatus;
import com.jogym.jogymapi.exception.CAlreadyUsingPtTicketException;
import com.jogym.jogymapi.exception.CMissingDataException;
import com.jogym.jogymapi.exception.CNotExistMemberOfStoreMemberException;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.ptticketbuyhistory.PtTicketBuyHistoryItem;
import com.jogym.jogymapi.model.ptticketbuyhistory.PtTicketBuyHistoryRequest;
import com.jogym.jogymapi.repository.PtTicketBuyHistoryRepository;
import com.jogym.jogymapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PtTicketBuyHistoryService {
    private final PtTicketBuyHistoryRepository ptTicketBuyHistoryRepository;

    public void setPtTicketBuyHistory(Member member, StoreMember storeMember, PtTicket ptTicket, PtTicketBuyHistoryRequest request) {
        if (!storeMember.equals(ptTicket.getStoreMember())) throw new CNotExistMemberOfStoreMemberException(); //트레이너가 해당 체육관 소속이 아니면 exception
        if (!storeMember.equals(member.getStoreMember())) throw new CNotExistMemberOfStoreMemberException(); //회원이 해당 체육관 소속이 아니면 exception
        if (!isNewTicketOfMember(member)) throw new CAlreadyUsingPtTicketException(); // 회원이 동일한 체육관에서 Pt를 등록할 때 exception

        PtTicketBuyHistory ptTicketBuyHistory = new PtTicketBuyHistory.PtTicketBuyHistoryBuilder(member, ptTicket, request).build();

        ptTicketBuyHistoryRepository.save(ptTicketBuyHistory);
    }

    public void putHistoryBuyStatus(StoreMember storeMember, long historyId, BuyStatus buyStatus) {
        PtTicketBuyHistory data = ptTicketBuyHistoryRepository.findByMember_StoreMemberAndId(storeMember, historyId).orElseThrow(CMissingDataException::new);

        data.putBuyStatus(buyStatus);

        ptTicketBuyHistoryRepository.save(data);
    }

    public ListResult<PtTicketBuyHistoryItem> getSeasonTicketBuyHistory(long memberId, StoreMember storeMember, int page) {
        Page<PtTicketBuyHistory> originData = ptTicketBuyHistoryRepository.findAllByMember_IdAndMember_StoreMemberOrderByIdDesc(memberId, storeMember, ListConvertService.getPageable(page));

        List<PtTicketBuyHistoryItem> result = new LinkedList<>();

        originData.getContent().forEach(e->{
            PtTicketBuyHistoryItem item = new PtTicketBuyHistoryItem.Builder(e).build();

            result.add(item);
        });
        return ListConvertService.settingResult(result, originData.getTotalElements(), originData.getTotalPages(), originData.getPageable().getPageNumber());
    }

    private boolean isNewTicketOfMember(Member member) {
        long dupTicket = ptTicketBuyHistoryRepository.countByMemberAndBuyStatus(member, BuyStatus.VALID);
        return dupTicket < 1;
    }
}
