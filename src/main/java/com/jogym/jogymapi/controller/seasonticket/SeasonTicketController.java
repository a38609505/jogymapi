package com.jogym.jogymapi.controller.seasonticket;

import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.model.common.CommonResult;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.seasonticket.SeasonTicketItem;
import com.jogym.jogymapi.model.seasonticket.SeasonTicketRequest;
import com.jogym.jogymapi.model.seasonticket.SeasonTicketUpdateRequest;
import com.jogym.jogymapi.service.storemember.ProfileService;
import com.jogym.jogymapi.service.storemember.StoreMemberService;
import com.jogym.jogymapi.service.common.ResponseService;
import com.jogym.jogymapi.service.seasonticket.SeasonTicketService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "정기권 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/season-ticket")
public class SeasonTicketController {
    private final SeasonTicketService seasonTicketService;
    private final StoreMemberService storeMemberService;
    private final ProfileService profileService;
    @ApiOperation(value = "정기권 등록")
    @PostMapping("/data")
    public CommonResult setSeasonTicket(@RequestBody @Valid SeasonTicketRequest request) { // 등록시키는 데 필요한 정보들을 담아준다.

        // 토큰을 주면 가맹점을 가져옴
        StoreMember storeMember = profileService.getMemberData();

        seasonTicketService.setSeasonTicket(storeMember, request);
        // 이 기능을 사용하기 위해서 필요한 것들을 담아준다.

        return ResponseService.getSuccessResult();
        // return 타입으로 성공하였을 때 성공했다는 문구를 보여주기 위해 getSuccessResult를 사용한다.
    }

    @ApiOperation(value = "정기권 리스트")
    @GetMapping("/list")
    public ListResult<SeasonTicketItem> getSeasonTicket(@RequestParam(value = "page", required = false, defaultValue = "1")int page) {
        // 토큰을 주면 가맹점을 가져옴
        StoreMember storeMember = profileService.getMemberData();
        // List를 만들 때 가공하여 사용할 도구를 쓰고, RequestParam을 사용해 value값을 page로 쓰고 필수임을 알려주며 page의 타입은 int라는 것을 알려준다.
        return ResponseService.getListResult(seasonTicketService.getSeasonTicket(storeMember, page), true);
        // return을 했을 때 어떤 것을 건네줄 것인 지 알려준다.
    }

    @ApiOperation(value = "정기권 수정")
    @PutMapping("/{SeasonTicketId}")
    public CommonResult putSeasonTicket(@PathVariable long SeasonTicketId, @RequestBody @Valid SeasonTicketUpdateRequest request) {
        // 기능을 사용하기 위해 필요한 자료들을 준비한다.

        seasonTicketService.putSeasonTicket(SeasonTicketId, profileService.getMemberData(), request);
        // 기능을 사용하기 위해 준비해뒀던 자료들을 사용

        return ResponseService.getSuccessResult();
        // return으로 getSuccessResult주고 성공했다는 문구를 건네준다.
    }

    @ApiOperation(value = "정기권 삭제")
    @PutMapping("/ticket-id/{TicketId}")
    public CommonResult putSeasonTicketDelete(@PathVariable long TicketId) { // 전체 다 삭제할 것이 아닌 특정 자료만 삭제할 것이기 떄문에 id를 선택해 삭제하는 방식으로 사용하기 위해 id를 받아준다.

        seasonTicketService.putSeasonTicketDelete(TicketId, profileService.getMemberData());
        // 기능을 사용하기 위해 준비해뒀던 자료를 사용한다.

        return ResponseService.getSuccessResult();
        // return으로 getSuccessResult를 주어 성공했다는 문구를 남겨준다.
    }
}
