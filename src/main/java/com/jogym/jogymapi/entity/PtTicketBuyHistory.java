package com.jogym.jogymapi.entity;

import com.jogym.jogymapi.enums.BuyStatus;
import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import com.jogym.jogymapi.model.ptticketbuyhistory.PtTicketBuyHistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PtTicketBuyHistory {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 등록일시
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 회원id
    @JoinColumn(name = "memberId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Member member;

    // PT권
    @JoinColumn(name = "ptTicketId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private PtTicket ptTicket;

    // 최대횟수
    @Column(nullable = false)
    private Integer maxCount;

    // 사용횟수
    @Column(nullable = false)
    private Integer usedCount;

    // 잔여횟수
    @Column(nullable = false)
    private Integer remainCount;

    // 상태
    @Column(nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private BuyStatus buyStatus;

    public void putBuyStatus(BuyStatus buyStatus){
        this.buyStatus = buyStatus;
    }

    private PtTicketBuyHistory(PtTicketBuyHistoryBuilder builder) {
        this.dateCreate = builder.dateCreate;
        this.member = builder.member;
        this.ptTicket = builder.ptTicket;
        this. maxCount = builder.maxCount;
        this.usedCount = builder.usedCount;
        this.remainCount = builder.remainCount;
        this.buyStatus = builder.buyStatus;
    }

    public static class PtTicketBuyHistoryBuilder implements CommonModelBuilder<PtTicketBuyHistory> {

        private final LocalDateTime dateCreate;
        private final Member member;
        private final PtTicket ptTicket;
        private final Integer maxCount;
        private final Integer usedCount;
        private final Integer remainCount;
        private final BuyStatus buyStatus;

        public PtTicketBuyHistoryBuilder(Member member, PtTicket ptTicket, PtTicketBuyHistoryRequest request) {
            this.dateCreate = LocalDateTime.now();
            this.member = member;
            this.ptTicket = ptTicket;
            this.maxCount = request.getMaxCount();
            this.usedCount = 0;
            this.remainCount = request.getMaxCount();
            this.buyStatus = BuyStatus.VALID;
        }

        @Override
        public PtTicketBuyHistory build() {
            return new PtTicketBuyHistory(this);
        }
    }
}
