package com.jogym.jogymapi.entity;

import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import com.jogym.jogymapi.model.boardreview.BoardReviewRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardReview {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 등록일시
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 가맹점id
    @JoinColumn(name = "storeMemberId")
    @ManyToOne(fetch = FetchType.LAZY)
    private StoreMember storeMember;

    // 제목
    @Column(nullable = false, length = 50)
    private String title;

    // 작성자
    @Column(nullable = false, length = 20)
    private String name;

    // 본문
    @Column(nullable = false, columnDefinition = "TEXT")
    private String content;

    private BoardReview(BoardReviewBuilder builder) {
        this.dateCreate = builder.dateCreate;
        this.storeMember = builder.storeMember;
        this.title = builder.title;
        this.name = builder.name;
        this.content = builder.content;
    }

    public static class BoardReviewBuilder implements CommonModelBuilder<BoardReview> {

        private final LocalDateTime dateCreate;
        private final StoreMember storeMember;
        private final String title;
        private final String name;
        private final String content;

        private BoardReviewBuilder(StoreMember storeMember, BoardReviewRequest request) {
            this.dateCreate = LocalDateTime.now();
            this.storeMember = storeMember;
            this.title = request.getTitle();
            this.name = request.getName();
            this.content = request.getContent();
        }

        @Override
        public BoardReview build() {
            return new BoardReview(this);
        }
    }
}
