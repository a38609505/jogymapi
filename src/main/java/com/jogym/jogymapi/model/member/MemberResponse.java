package com.jogym.jogymapi.model.member;

import com.jogym.jogymapi.entity.Member;
import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import com.jogym.jogymapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MemberResponse {
    // 등록일, 회원명, 연락처, 주소, 성별, 생년월일, 비고
    @ApiModelProperty()
    private String dateCreate;

    private String name;

    private String phoneNumber;

    private String address;

    private String gender;

    private LocalDate dateBirth;

    private String memo;

    private MemberResponse(Builder builder){
        this.dateCreate = builder.dateCreate;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
        this.address = builder.address;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.memo = builder.memo;
    }


    public static class Builder implements CommonModelBuilder<MemberResponse>{
        private final String dateCreate;
        private final String name;
        private final String phoneNumber;
        private final String address;
        private final String gender;
        private final LocalDate dateBirth;
        private final String memo;

        public Builder(Member member){
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(member.getDateCreate());
            this.name = member.getName();
            this.phoneNumber = member.getPhoneNumber();
            this.address = member.getAddress();
            this.gender = member.getGender().getName();
            this.dateBirth = member.getDateBirth();
            this.memo = member.getMemo();
        }

        @Override
        public MemberResponse build() {
            return new MemberResponse(this);
        }
    }

}
