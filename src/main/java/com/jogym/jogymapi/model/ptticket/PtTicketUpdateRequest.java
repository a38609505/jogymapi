package com.jogym.jogymapi.model.ptticket;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PtTicketUpdateRequest {
    @NotNull
    @Min(0)
    @Max(12)
    @ApiModelProperty(notes = "최대월", required = true)
    private Integer maxCount;

    @NotNull
    @ApiModelProperty(notes = "요금", required = true)
    private Double unitPrice;
}
