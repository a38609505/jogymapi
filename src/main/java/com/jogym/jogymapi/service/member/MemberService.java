package com.jogym.jogymapi.service.member;

import com.jogym.jogymapi.entity.Member;
import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.exception.CMissingDataException;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.member.MemberItem;
import com.jogym.jogymapi.model.member.MemberRequest;
import com.jogym.jogymapi.model.member.MemberResponse;
import com.jogym.jogymapi.model.member.MemberUpdateRequest;
import com.jogym.jogymapi.repository.MemberRepository;
import com.jogym.jogymapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }


    // 가맹점Id( StoreMemberId )와 MemberRequest를 받아와 Builder처리를 함.
    // 맴버 등록
    public void setMember(StoreMember storeMember, MemberRequest request) {
        Member member = new Member.MemberBuilder(storeMember, request).build();

        memberRepository.save(member);
    }

    // result(결과).add(더하다)(new 원본아이템.Builder(원본).build());
    // for (원본 원본 : originList.getContent())
    // List<원본아이템> result = new LinkedList<>();
    // Page(원본) originList = Repository에서 Page처리한것을 가져오기.
    // 가맹점Id( StoreMemberId )와 Repository에서 Page처리를 한 것을 가져오기
    // Repository에서 페이징 처리를 할 수 있도록 적기.
    // 맴버 리스트 ( 페이징 )
    public ListResult<MemberItem> getMemberList(StoreMember storeMember, int page) {
        Page<Member> originList = memberRepository.findAllByStoreMemberAndIsEnabledOrderByDateCreateDesc(storeMember, true,  ListConvertService.getPageable(page));
        List<MemberItem> result = new LinkedList<>();

        for (Member member : originList.getContent()) {
            result.add(new MemberItem.Builder(member).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }


    public MemberResponse getMember(long memberId, StoreMember storeMember) {
        Member member = memberRepository.findByIdAndStoreMember(memberId,storeMember).orElseThrow(CMissingDataException::new); // 데이터가 없습니다. 데이터가 없으면 던지기.
        if(!member.getIsEnabled()) throw new CMissingDataException(); // 탈퇴된 회원 입니다. 탈퇴된 회원이면 던지기.
        return new MemberResponse.Builder(member).build();
    }

    public void putMember(long memberId, StoreMember storeMember, MemberUpdateRequest request) {
        Member originData = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        if(!originData.getIsEnabled()) throw new CMissingDataException(); // 탈퇴된 회원 입니다. 탈퇴된 회원이면 던지기.
        if(originData.getStoreMember().getId() != storeMember.getId()) throw new CMissingDataException(); // 가맹점 정보가 다릅니다. 가맹점 정보가 다르면 던지기.
        originData.putMember(request);
        memberRepository.save(originData);
    }

    public void putMemberDelete(long memberId, StoreMember storeMember) {
        Member originData = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        if(!originData.getIsEnabled()) throw new CMissingDataException(); // 탈퇴된 회원 입니다. 탈퇴된 회원이면 던지기
        if(originData.getStoreMember().getId() != storeMember.getId()) throw new CMissingDataException(); // 가맹점 정보가 다릅니다. 가맹점 정보가 다르면 던지기.
        originData.putMemberDelete();
        memberRepository.save(originData);
    }
}
