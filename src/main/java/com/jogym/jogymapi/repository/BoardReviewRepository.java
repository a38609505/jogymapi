package com.jogym.jogymapi.repository;

import com.jogym.jogymapi.entity.BoardReview;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardReviewRepository extends JpaRepository<BoardReview, Long> {
}
