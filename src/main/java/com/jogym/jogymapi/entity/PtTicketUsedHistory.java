package com.jogym.jogymapi.entity;

import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import com.jogym.jogymapi.model.ptticketusedhistory.PtTicketUsedHistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PtTicketUsedHistory {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateCreateDay;

    // 등록시
    @Column(nullable = false)
    private LocalTime dateCreateTime;

    // PT권 구매내역 id
    @JoinColumn(name = "ptTicketBuyHistoryId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private PtTicketBuyHistory ptTicketBuyHistory;

    // 최대횟수
    @Column(nullable = false)
    private Integer maxCount;

    // 사용횟수
    @Column(nullable = false)
    private Integer usedCount;

    // 잔여횟수
    @Column(nullable = false)
    private Integer remainCount;

    private PtTicketUsedHistory(PtTicketUsedHistoryBuilder builder) {
        this.dateCreateDay = builder.dateCreateDay;
        this.dateCreateTime = builder.dateCreateTime;
        this.ptTicketBuyHistory = builder.ptTicketBuyHistory;
        this.maxCount = builder.remainCount;
        this.usedCount = builder.usedCount;
        this.remainCount = builder.remainCount;
    }

    public static class PtTicketUsedHistoryBuilder implements CommonModelBuilder<PtTicketUsedHistory> {

        private LocalDateTime dateCreateDay;
        private LocalTime dateCreateTime;
        private PtTicketBuyHistory ptTicketBuyHistory;
        private Integer maxCount;
        private Integer usedCount;
        private Integer remainCount;

        public PtTicketUsedHistoryBuilder(PtTicketBuyHistory ptTicketBuyHistory, PtTicketUsedHistoryRequest request) {
            this.dateCreateDay = LocalDateTime.now();
            this.dateCreateTime = LocalTime.now();
            this.ptTicketBuyHistory = ptTicketBuyHistory;
            this.maxCount = request.getMaxCount();
            this.usedCount = request.getUsedCount();
            this.remainCount = request.getRemainCount();
        }


        @Override
        public PtTicketUsedHistory build() {
            return new PtTicketUsedHistory(this);
        }
    }
}
