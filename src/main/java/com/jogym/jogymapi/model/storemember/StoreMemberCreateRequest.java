package com.jogym.jogymapi.model.storemember;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class StoreMemberCreateRequest {

    @ApiModelProperty(notes = "회원등록일", required = true)
    @NotNull
    private LocalDateTime dateCreate;

    @ApiModelProperty(notes = "아이디(5~30)", required = true)
    @NotNull
    @Length(min = 5, max = 30)
    private String username;

    @ApiModelProperty(notes = "비밀 번호(8~20)", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String password;

    @ApiModelProperty(notes = "비밀 번호 확인(8~20)", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String passwordRe;

    @ApiModelProperty(notes = "이름(2~20)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @ApiModelProperty(notes = "연락처(13)", required = true)
    @NotNull
    @Length(min = 13, max = 13)
    private String phoneNumber;

    @ApiModelProperty(notes = "사업자 등록번호(12)", required = true)
    @NotNull
    @Length(min = 12, max = 12)
    private String busineesNumber;

    @ApiModelProperty(notes = "가맹점명(2~50)", required = true)
    @NotNull
    @Length(min = 2, max = 50)
    private String storeName;

    @ApiModelProperty(notes = "주소(2~100)", required = true)
    @NotNull
    @Length(min = 2, max = 100)
    private String address;

}
