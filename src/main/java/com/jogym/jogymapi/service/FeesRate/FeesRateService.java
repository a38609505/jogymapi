package com.jogym.jogymapi.service.FeesRate;

import com.jogym.jogymapi.entity.FeesRate;
import com.jogym.jogymapi.exception.CMissingDataException;
import com.jogym.jogymapi.model.FeesRate.FeesRateItem;
import com.jogym.jogymapi.model.FeesRate.FeesRateRequest;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.repository.FeesRateRepository;
import com.jogym.jogymapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FeesRateService {
    private final FeesRateRepository feesRateRepository;

    // LocalDateTime.now와 Double은 0D가 entity에서 나옴.
    // 토큰을 받으나 ProfileService가 필요없음.
    // entity에서 Request가 필요없어 Service에서도 필요가 없어 적지 않음.
    // ProfileService가 필요하지 않음.
    // 수수료율 등록 서비스
    public void setFeesRate(FeesRateRequest request) {
        FeesRate feesRate = new FeesRate.FeesRateBuilder(request).build();

        feesRateRepository.save(feesRate);
    }

    // }); 다음에 return 돌리기.
    // originList.forEach(e -> { result.add(new 원본아이템.원본빌더(e).build());
    // List<원본아이템> result = new LinkedList<>();
    // List<원본> originList = 원본Repository.findAll(); --> 어차피 다 보여 줄 것이기 때문에 findAll로 다 보여준다.
    // ListResult<원본아이템> get원본() { --> List가 아닌 ListResult로 해야만 함.
    // 수수료율 리스트
    public ListResult<FeesRateItem> getFeesRate() {
        List<FeesRate> originList = feesRateRepository.findAll();
        List<FeesRateItem> result = new LinkedList<>();
        originList.forEach(e -> {
            result.add(new FeesRateItem.FeesRateBuilder(e).build());
        });
        return ListConvertService.settingResult(result);
    }

    // 만약 Long Id를 받지 않는다면 전체 정보 모두를 수정하는 꼴이 되어버림.
    // 수정에서 Long Id를 받는 이유는 한개의 정보만 수정할 것이기 때문.
    // ProfileService가 필요하지 않음.
    // 수수료율 수정
    public void putFeesRate(long feesRateId, FeesRateRequest request) {
        FeesRate feesRate = feesRateRepository.findById(feesRateId).orElseThrow(CMissingDataException::new); // 수수료율 정보가 없습니다. 수수료율 정보가 다르면 던지기.
        feesRate.putFeesRate(request);
        feesRateRepository.save(feesRate);
    }

    public void delFeesRate(long feesRateId) {
        feesRateRepository.deleteById(feesRateId);
    }
}
