package com.jogym.jogymapi.model.storemember;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberUpdateRequest {
    @ApiModelProperty(notes = "이름(2~20)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @ApiModelProperty(notes = "연락처(13)", required = true)
    @NotNull
    @Length(min = 13, max = 13)
    private String phoneNumber;

    @ApiModelProperty(notes = "면허증 번호(15)", required = true)
    @NotNull
    @Length(min = 15, max = 15)
    private String licenseNumber;
}
