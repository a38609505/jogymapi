package com.jogym.jogymapi.entity;

import com.jogym.jogymapi.enums.BuyStatus;
import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SeasonTicketBuyHistory {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 등록일시
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 회원 id
    @JoinColumn(name = "memberId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Member member;

    // 정기권 id
    @JoinColumn(name = "seasonTicketId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private SeasonTicket seasonTicket;

     // 시작일
    @Column(nullable = false)
    private LocalDate dateStart;

    // 종료일
    @Column(nullable = false)
    private LocalDate dateEnd;

    // 최근방문일
    private LocalDate dateLast;

    // 상태
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 10)
    private BuyStatus buyStatus;

    public void putBuyStatus(BuyStatus buyStatus){
        this.buyStatus = buyStatus;
    }

    public void putDateLast(){
        this.dateLast = LocalDate.now();
    }

    private SeasonTicketBuyHistory(SeasonTicketBuyHistoryBuilder builder) {
        this.dateCreate = builder.dateCreate;
        this.member = builder.member;
        this.seasonTicket = builder.seasonTicket;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
        this.dateLast = builder.dateLast;
        this.buyStatus = builder.buyStatus;
    }

    public static class SeasonTicketBuyHistoryBuilder implements CommonModelBuilder<SeasonTicketBuyHistory> {
        private final LocalDateTime dateCreate;
        private final Member member;
        private final SeasonTicket seasonTicket;
        private final LocalDate dateStart;
        private final LocalDate dateEnd;
        private final LocalDate dateLast;
        private final BuyStatus buyStatus;

        public SeasonTicketBuyHistoryBuilder(Member member, SeasonTicket seasonTicket) {
            this.dateCreate = LocalDateTime.now();
            this.member = member;
            this.seasonTicket = seasonTicket;
            this.dateStart = LocalDate.now();
            this.dateEnd = LocalDate.now().plusMonths(seasonTicket.getMaxMonth());
            this.dateLast = LocalDate.now();
            this.buyStatus = BuyStatus.VALID;
        }

        @Override
        public SeasonTicketBuyHistory build() {
            return new SeasonTicketBuyHistory(this);
        }
    }
}
