package com.jogym.jogymapi.controller.seasonticketusedhistory;

import com.jogym.jogymapi.entity.Member;
import com.jogym.jogymapi.entity.SeasonTicketBuyHistory;
import com.jogym.jogymapi.model.common.CommonResult;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.seasonticketusedhistory.SeasonTicketUsedHistoryItem;
import com.jogym.jogymapi.service.common.ResponseService;
import com.jogym.jogymapi.service.member.MemberService;
import com.jogym.jogymapi.service.seasonticket.SeasonTicketService;
import com.jogym.jogymapi.service.seasonticketbuyhistory.SeasonTicketBuyHistoryService;
import com.jogym.jogymapi.service.seasonticketusedhistory.SeasonTicketUsedHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Api(tags = "정기권 이용내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/season-ticket-used-history")
public class SeasonTicketUsedHistoryController {
    private final SeasonTicketUsedHistoryService seasonTicketUsedHistoryService;
    private final SeasonTicketBuyHistoryService seasonTicketBuyHistoryService;
    private final MemberService memberService;

    @ApiOperation(value = "정기권 이용내역 등록")
    @PostMapping("/season-ticket-buy-history-id/{SeasonTicketBuyHistoryId}")
    public CommonResult setSeasonTicketUsedHistory(@PathVariable long SeasonTicketBuyHistoryId) { // 정기권 이용내역을 등록시키기 위해선 정기권 구매내역 id가 필요하다.

         SeasonTicketBuyHistory seasonTicketBuyHistory = seasonTicketBuyHistoryService.getData(SeasonTicketBuyHistoryId);
        // 정기권 구매내역 id를 가져오기 위해 seasonTicketBuyHistoryService에 id를 주면 return으로 원본을 주는 기능을 만들고 가져와서 사용한다.

        seasonTicketUsedHistoryService.setSeasonTicketUsedHistory(seasonTicketBuyHistory);
        // 정기권 이용내역 서비스를 끌고와 등록 기능으로 이름 정의해두었던 setSeasonTicketUsedHistory를 가져오고 은
        // 기능을 수행시키는 데 있어 필요하기 때문에 아이디를 주면 원본을 주는 친구를 통해 얻은 구매내역 원본을 가져와 칸에 채워준다.

        return ResponseService.getSuccessResult();
        // 소스코드가 온전히 잘 수행되었을 때 SuccessResult에 담긴 성공했다는 메시지를 넘긴다.
    }

    @ApiOperation(value = "정기권 이용내역 리스트")
    @GetMapping("/member-id/{memberId}/season-ticket-id/{seasonTicketId}")
    public ListResult<SeasonTicketUsedHistoryItem> getData(@RequestParam(value = "page", required = false, defaultValue = "1")int page, @PathVariable long memberId) {
        Member member = memberService.getData(memberId);

        return ResponseService.getListResult(seasonTicketUsedHistoryService.getData(page, member), true);
    }
}
