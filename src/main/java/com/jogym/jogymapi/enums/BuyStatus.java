package com.jogym.jogymapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BuyStatus {
    VALID("유효"),
    COMPLETE("완료");

    private final String name;

    }
