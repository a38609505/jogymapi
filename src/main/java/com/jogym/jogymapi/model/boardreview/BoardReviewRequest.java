package com.jogym.jogymapi.model.boardreview;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BoardReviewRequest {
    @NotNull
    @Length(min = 2, max = 50)
    @ApiModelProperty(notes = "제목", required = true)
    private String title;

    @NotNull
    @Length(min = 2, max = 20)
    @ApiModelProperty(notes = "작성자", required = true)
    private String name;

    @NotNull
    @ApiModelProperty(notes = "본문", required = true)
    private String content;
}
