package com.jogym.jogymapi.model.ptticketbuyhistory;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PtTicketBuyHistoryRequest {

    @ApiModelProperty(name = "회원 시퀀스 번호", required = true)
    @NotNull
    @Min(0)
    private Long memberId;

    @ApiModelProperty(name = "PT권 시퀀스 번호", required = true)
    @NotNull
    @Min(0)
    private Long ptTicketId;

    @NotNull
    @Min(1)
    @Max(1000)
    @ApiModelProperty(notes = "최대횟수(1~1000)",  required = true)
    private Integer maxCount;

}
