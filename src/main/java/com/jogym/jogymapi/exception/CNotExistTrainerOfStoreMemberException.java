package com.jogym.jogymapi.exception;

public class CNotExistTrainerOfStoreMemberException extends RuntimeException {
    public CNotExistTrainerOfStoreMemberException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotExistTrainerOfStoreMemberException(String msg) {
        super(msg);
    }

    public CNotExistTrainerOfStoreMemberException() {
        super();
    }
}
