package com.jogym.jogymapi.controller.FeesRate;

import com.jogym.jogymapi.model.FeesRate.FeesRateItem;
import com.jogym.jogymapi.model.FeesRate.FeesRateRequest;
import com.jogym.jogymapi.model.common.CommonResult;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.service.FeesRate.FeesRateService;
import com.jogym.jogymapi.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "수수료율")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/fees-rate")
public class FeesRateController {
    private final FeesRateService feesRateService;

    @ApiOperation(value = "수수료율 등록")
    @PostMapping("/data")
    public CommonResult setFeesRate(@RequestBody @Valid FeesRateRequest request) {
        feesRateService.setFeesRate(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "수수료율 전체")
    @GetMapping("/all")
    public ListResult<FeesRateItem> getFeesRate() {
        return ResponseService.getListResult(feesRateService.getFeesRate(), true);
    }

    @ApiOperation(value = "수수료율 수정")
    @PutMapping("/{feesRateId}")
    public CommonResult putFeesRate(@PathVariable long feesRateId, @RequestBody @Valid FeesRateRequest request) {
        feesRateService.putFeesRate(feesRateId, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "수수료율 삭제")
    @DeleteMapping("/{feesRateId}")
    public CommonResult delFeesRate(@PathVariable long feesRateId) {
        feesRateService.delFeesRate(feesRateId);

        return ResponseService.getSuccessResult();
    }
}
