package com.jogym.jogymapi.repository;

import com.jogym.jogymapi.entity.FeesRate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeesRateRepository extends JpaRepository<FeesRate, Long> {
}
