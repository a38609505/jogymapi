package com.jogym.jogymapi.service.storemember;

import com.jogym.jogymapi.configure.JwtTokenProvider;
import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.enums.MemberGroup;
import com.jogym.jogymapi.exception.CMissingDataException;
import com.jogym.jogymapi.exception.CNotMatchPasswordException;
import com.jogym.jogymapi.model.storemember.LoginRequest;
import com.jogym.jogymapi.model.storemember.LoginResponse;
import com.jogym.jogymapi.repository.StoreMemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final StoreMemberRepository storeMemberRepository;
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;

    // 로그인 타입은 WEB or APP (WEB인경우 토큰 유효시간 10시간, APP은 1년)
    public LoginResponse doLogin(MemberGroup memberGroup, LoginRequest loginRequest, String loginType) {
        StoreMember storeMember = storeMemberRepository.findByUsername(loginRequest.getUsername()).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다 던지기

        if (!storeMember.getIsEnabled()) throw new CMissingDataException(); // 회원탈퇴인 경우인데 DB에 남아있으므로 이걸 안 들키려면 회원정보가 없습니다 이걸로 던져야 함.
        if (!storeMember.getMemberGroup().equals(memberGroup)) throw new CMissingDataException(); // 일반회원이 최고관리자용으로 로그인하려거나 이런 경우이므로 메세지는 회원정보가 없습니다로 일단 던짐.
        if (!passwordEncoder.matches(loginRequest.getPassword(), storeMember.getPassword())) throw new CNotMatchPasswordException(); // 비밀번호가 일치하지 않습니다 던짐

        String token = jwtTokenProvider.createToken(String.valueOf(storeMember.getUsername()), storeMember.getMemberGroup().toString(), loginType);

        return new LoginResponse.LoginResponseBuilder(token, storeMember.getName()).build();
    }
}
