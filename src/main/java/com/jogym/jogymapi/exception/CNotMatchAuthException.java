package com.jogym.jogymapi.exception;

public class CNotMatchAuthException extends RuntimeException {
    public CNotMatchAuthException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotMatchAuthException(String msg) {
        super(msg);
    }

    public CNotMatchAuthException() {
        super();
    }
}
