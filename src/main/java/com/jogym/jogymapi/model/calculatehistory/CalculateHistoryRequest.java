package com.jogym.jogymapi.model.calculatehistory;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CalculateHistoryRequest {
    @ApiModelProperty(notes = "등록년", required = true)
    @NotNull
    private Integer dateCreateYear;

    @ApiModelProperty(notes = "등록월", required = true)
    @NotNull
    private Integer dateCreateMonth;

    @ApiModelProperty(notes = "총 매출금", required = true)
    @NotNull
    private Double totalPrice;

    @ApiModelProperty(notes = "공제금", required = true)
    @NotNull
    private Double minusPrice;

    @ApiModelProperty(notes = "정산금", required = true)
    @NotNull
    @Max(1000)
    private Double calculatePrice;

}
