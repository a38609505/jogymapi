package com.jogym.jogymapi.entity;

import com.jogym.jogymapi.enums.CalculateStatus;
import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import com.jogym.jogymapi.model.calculatehistory.CalculateHistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CalculateHistory {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 등록년
    @Column(nullable = false)
    private Integer dateCreateYear;

    // 등록월
    @Column(nullable = false)
    private Integer dateCreateMonth;

    // 가맹점
    @JoinColumn(name = "storeMemberId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private StoreMember storeMember;

    // 총 매출금
    @Column(nullable = false)
    private Double totalPrice;

    // 수수료율
    @Column(nullable = false)
    private Double feesRate;

    // 공제금
    @Column(nullable = false)
    private Double minusPrice;

    // 정산금
    @Column(nullable = false)
    private Double calculatePrice;

    // 정산 상태
    @Column(nullable = false, length = 1000)
    @Enumerated(EnumType.STRING)
    private CalculateStatus calculateStatus;

    private CalculateHistory(CalculateHistoryBuilder builder) {
        this.dateCreateYear = builder.dateCreateYear;
        this.dateCreateMonth = builder.dateCreateMonth;
        this.totalPrice = builder.totalPrice;
        this.minusPrice = builder.minusPrice;
        this.calculatePrice = builder.calculatePrice;
        this.calculateStatus = builder.calculateStatus;
    }

    public static class CalculateHistoryBuilder implements CommonModelBuilder<CalculateHistory> {

        private final Integer dateCreateYear;
        private final Integer dateCreateMonth;
        private final Double totalPrice;
        private final Double minusPrice;
        private final Double calculatePrice;
        private final CalculateStatus calculateStatus;

        public CalculateHistoryBuilder(CalculateStatus calculateStatus, CalculateHistoryRequest request) {
            this.dateCreateYear = request.getDateCreateYear();
            this.dateCreateMonth = request.getDateCreateMonth();
            this.totalPrice = request.getTotalPrice();
            this.minusPrice = request.getMinusPrice();
            this.calculatePrice = request.getCalculatePrice();
            this.calculateStatus = calculateStatus;
        }

        @Override
        public CalculateHistory build() {
            return new CalculateHistory(this);
        }
    }

}
