package com.jogym.jogymapi.service.storemember;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
@RequiredArgsConstructor
public class AuthPhoneService {

    // 엄....예린이형 로직 코드 참고해서 넣기..
    private String makeRandomNumber() {
        Random random = new Random(); // 랜덤 숫자 생성
        int createNum = 0; // 하나의 숫자를 만들어서 기본값 0으로 초기화 / createNum은 한 자리의 랜덤 숫자를 만듦
        String ranNum = ""; // 랜덤 숫자가 들어갈 자리 생성
        String resultNum = ""; // 결과 숫자가 들어갈 자리 생성

        for (int i=0; i<6; i++) { // int 타입으로 줘서 i=0는 초기값으로 설정하고 i<6 은 6번 돈다는 걸 의미하고 i++ 초기값으로 설정된 사이클 횟수를 늘려준다.
                                    // 얘가 6자리를 만들기 위해선 하나의 랜덤숫자를 6번 반복해야됨
            createNum = random.nextInt(9); // 0부터 9까지의 랜덤 숫자를 생성함 for문이니까 반복
            ranNum = Integer.toString(createNum); // 나온 int타입의 랜덤 숫자를 String 타입으로 줘서 문자로 바꿈
                                                    // 6자리로 랜덤숫자를 만들어야 하는데 숫자끼리 더하면 6자리가 나오지 않고 문자로 바꿔야 6자리로 나열할 수 있기 때문에 int타입을 String 타입으로 변환한다. 1 + 2 = 3 / "1"+"2" = 12
            resultNum += ranNum; // 결과 값은 랜덤으로 나온 숫자 6개를 전부 더한 값이다.
        }
        return resultNum; // resultNum 를 return한다.
    }
}
