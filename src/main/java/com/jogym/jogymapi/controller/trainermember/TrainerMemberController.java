package com.jogym.jogymapi.controller.trainermember;

import com.jogym.jogymapi.entity.StoreMember;
import com.jogym.jogymapi.model.common.CommonResult;
import com.jogym.jogymapi.model.common.ListResult;
import com.jogym.jogymapi.model.trainermember.TrainerMemberItem;
import com.jogym.jogymapi.model.trainermember.TrainerMemberRequest;
import com.jogym.jogymapi.model.trainermember.TrainerMemberUpdateRequest;
import com.jogym.jogymapi.service.common.ResponseService;
import com.jogym.jogymapi.service.storemember.ProfileService;
import com.jogym.jogymapi.service.trainermember.TrainerMemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "트레이너 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/trainer-member")
public class TrainerMemberController {
    private final TrainerMemberService trainerMemberService;
    private final ProfileService profileService;

    @ApiOperation(value = "트레이너 등록")
    @PostMapping("/data")
    public CommonResult setTrainer(@RequestBody @Valid TrainerMemberRequest request) {
        trainerMemberService.setTrainer(profileService.getMemberData(), request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "트레이너 리스트")
    @GetMapping("/list")
    public ListResult<TrainerMemberItem> getTrainerList(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        StoreMember storeMember = profileService.getMemberData();
        return ResponseService.getListResult(trainerMemberService.getTrainerList(storeMember,page), true);
    }

    @ApiOperation(value = "트레이너 상세정보")
    @GetMapping("/{trainerId}")
    public CommonResult getTrainer(@PathVariable long trainerId) {
        StoreMember storeMember = profileService.getMemberData();
        return ResponseService.getSingleResult(trainerMemberService.getTrainer(trainerId, storeMember));
    }


    @ApiOperation(value = "트레이너 정보 수정")
    @PutMapping("/{trainerId}")
    public CommonResult putTrainer(@PathVariable long trainerId, @RequestBody @Valid TrainerMemberUpdateRequest request) {
        trainerMemberService.putTrainer(trainerId, profileService.getMemberData(), request);
        return ResponseService.getSuccessResult();
    }


    // 상태만 변경 하면 되기는 하나 상태를 변경을 해보았음.
    // service에서는 true에서 false로 바꿨으나 Controller에서는 바꾸질 못함.
    // 내가 보기에는 빌더 문제로 여겨짐..
    // 열심히 찾아 보기는 하였 으나 찾지 못하고 회원으로 넘어 가버렸음..
    @ApiOperation(value = "트레이너 삭제")
    @PutMapping("/trainer-member-id/{trainerMemberId}")
    public CommonResult putTrainerDelete(@PathVariable long trainerMemberId) {
        trainerMemberService.putTrainerDelete(trainerMemberId, profileService.getMemberData());
        return ResponseService.getSuccessResult();
    }
}
