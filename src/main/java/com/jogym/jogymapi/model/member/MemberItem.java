package com.jogym.jogymapi.model.member;

import com.jogym.jogymapi.entity.Member;
import com.jogym.jogymapi.interfaces.CommonModelBuilder;
import com.jogym.jogymapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

import static com.jogym.jogymapi.lib.CommonFormat.convertLocalDateTimeToString;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {
    @ApiModelProperty(notes = "시퀀스", required = true)
    @NotNull
    private Long id;

    @ApiModelProperty(notes = "등록일", required = true)
    @NotNull
    private String dateCreate;

    @ApiModelProperty(notes = "이름", required = true)
    @NotNull
    private String name;

    @ApiModelProperty(notes = "연락처", required = true)
    @NotNull
    private String phoneNumber;

    private MemberItem(Builder builder) {
        this.id = builder.id;
        this.dateCreate = builder.dateCrate;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
    }

    public static class Builder implements CommonModelBuilder<MemberItem> {
        private final Long id;
        private final String dateCrate;
        private final String name;
        private final String phoneNumber;

        public Builder(Member member) {
            this.id = member.getId();
            this.dateCrate = CommonFormat.convertLocalDateTimeToString(member.getDateCreate());
            this.name = member.getName();
            this.phoneNumber = member.getPhoneNumber();
        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}
