package com.jogym.jogymapi.repository;

import com.jogym.jogymapi.entity.StoreMember;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StoreMemberRepository extends JpaRepository<StoreMember, Long> {
    Optional<StoreMember> findByUsername(String username);

    long countByUsername(String username);

    Page<StoreMember> findAllByIdGreaterThanEqualOrderByIdDesc(long id, Pageable pageable);

    Optional<StoreMember>findByNameAndPhoneNumber(String name, String phoneNumber);

    Optional<StoreMember> findByUsernameAndNameAndPhoneNumber(String username, String name, String phoneNumber);
}
